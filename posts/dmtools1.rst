.. title: Dungeons and Pythons: a prelude
.. slug: Dungeons-and-Pythons:-a-prelude
.. date: 2018-08-25 Sat
.. tags: tag1
.. link: 
.. description: How python and org-mode made me a better Dungeon Master.
.. type: text



1 Being a dungeon master is hard.
---------------------------------

I play nerdy boardgames.
One of my favourites is the new, old kid-on-the-block, Dungeons and Dragons Fifth Edition.
If you've never had the joy of playing it yourself, allow me to explain the basic setup.
You, and a collection of people you (ideally) like, get together on a lazy weekend day and
put together your collective alter-ego as an adventuring party in a fantasy world.
The process of building these characters is described in the Player's Manual, a hefty
tome that describes in no short detail how to translate a series of dice rolls
(you own a twenty-sided die, right?) into a person with strengths and weaknesses - a character.
It's interesting in it's own right how a series of pseudo-random numbers can translate to a distinctly
personable traits and characteristic, but that's another blog post.
One person in this group decides to be (or is elected to be), the vaunted role of the "dungeon master".
While every other person in your group acts through the lens of their new character,
the dungeon master (or "DM") is the narrator of the world around the characters.
It's a tricky role, often requiring a deeper understanding of the rules and a 
knack for improvisation.
You become a verbally described window through which your players view the world,
on call to describe any character's immediate space and their interactions within it.
No pressure.

2 Sudden Depth
--------------

The beauty of roleplaying games is often in how elegantly they boil down reality.
Can one of your players shoot an arrow from a man's head at fifty paces?
Generally, the answer is **probably**. 
Usually it has to do with both the skills of the character and an element of random chance,
with many tests imaginary tests of skill boiling down to
"Add this number to the number rolled on this dice. Is it higher than this other number?"
Traditionally, this book-keeping is done by the dungeon master.
But book-keeping is not the point of these games - the point is to have fun and build a story!
I don't want to have to rummage through pages of notes to try and remember how hard it is
to beat the local town guard in a fistfight!
It is at this point I ask the question 
"Can I make a python script do this thing for me?"
and describe my attempts below.

3 Emacs and the tools of a trade
--------------------------------

I've recently been trying to do as much as I possibly can in the text-editor-turned-lifestyle
Emacs. I am a recent Vim refugee in only having made the switch sometime in May.
Someday I would like to wax poetic about Emacs but now's not the time.
One of my favourite things about Emacs is org-mode.
On the surface, its just another markup language.
Deeper down, its a beautiful framework for organising your entire life in bytesize plaintext files.
Further still, it can run code, making it very similar to a jupyter notebook.
I write much of my Dungeons and Dragons notes in an ever-growing .org file, and littered around it
are interactive codeblocks I can simply press Ctrl-C Ctrl-C on and have a python script bear
the mental load of "how strong is a bugbear" and let me think about more human questions like
"What does something called a bugbear even want?"
I've taken some pains to put together a neat little python module named dmtools.py
so that I can simply import it at the start of the file and call it's methods and functions
anywhere in my notes, open in front of me at all times during a session.

4 The code itself
-----------------

4.1 Setting the stage
~~~~~~~~~~~~~~~~~~~~~

Here are the packages I use.

- xdice is a useful little nice notation parser

- random is useful when you need a number - any number

- pickle is used to save tables and characters to disk for later!

- pandas and numpy are python's ubiquitous tabular data mani:Wpulation and numerical array libraries

.. code:: ipython

    import xdice as d
    import pandas as pd
    import numpy as np
    import random as rand
    import pickle
    pd.set_option('mode.chained_assignment', None)

The first piece of code is there to suppress some warnings Pandas is generating about
chained assignment - something I don't think is a problem in my code (although if you spot
a chained assignment, please tell me! I'm always open to brushing up my code.)

- (a list name in brackets, you'll find out why later)

- an item

- another item

- 5

- the last item in this list.

Emacs is, in effect a glorified emacs-lisp interpreter, and Org-Mode within it uses
elisp's lists.
Which means that any lists coming from inside emacs are going to look like

.. code:: ipython

    print(olist1)
