.. title: blah dot r s t
.. slug: thing-as-rst 
.. date: 2012-09-15 19:52
.. tags: 
.. link: 
.. description: 
.. type: text
.. author: richard

.. code:: common-lisp

    (directory-files ".")

.. code:: python

    from os import listdir
    return listdir(".")


.. code:: python

    from os import listdir
    return listdir(".")

.. code:: python

    from os import listdir
    return listdir(".")

::

    None



.. code:: bash

    ls

.. code:: python

    import random
    a = random.uniform(1,6)
    print(a)

heres a table.
it doesnt work without the tabulate package.

.. code:: python

    import pandas as pd
    import numpy as np
    a = np.arange(1,20)*4
    d1 = pd.DataFrame(a,columns=["col1"])
    return(d1) 

.. code:: python

    import numpy as np
    a = np.zeros(10)
    print(a)

.. code:: python

    import sys
    print(sys.executable)

.. table::
    :name: data_table

    +---+----+
    | a |  5 |
    +---+----+
    | c | 22 |
    +---+----+
    | d |  3 |
    +---+----+

.. code:: python

    # Return row specified by val.
    # In non-session mode, use return to return results.
    return(data)

::

    None

lets try ipython

.. code:: ipython

    # Return row specified by val.
    # In non-session mode, use return to return results.
    aaa = data # macros variables need to be assigned to, otherwise you're losing your tasty data
    aaa

this means i can narrow down the problem to the fact that the table i was referencing before
was inside a #results block, instead of a #name block or #tblname block
maybe drawers would fix this?

- thing1

- thing 2

- THING THREE

- THING 4!

.. code:: ipython

    # Return row specified by val.
    # In non-session mode, use return to return results.
    aaa = data # macros variables need to be assigned to, otherwise you're losing your tasty data
    aaa



yay!
